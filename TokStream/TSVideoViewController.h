//
//  TSVideoViewController.h
//

#import <UIKit/UIKit.h>
#import <Opentok/Opentok.h>
#import "TSConnectionModel.h"
#import "TSSettingsViewController.h"

@interface TSVideoViewController : UIViewController <OTSessionDelegate, OTSubscriberDelegate, OTPublisherDelegate>
- (void)doConnect;
- (void)doPublish;
- (void)showAlert:(NSString*)string;

@property (nonatomic, strong) TSConnectionModel* connectionInfo;
@property (weak, nonatomic) IBOutlet UILabel *statusIndicator;

@end
