//
//  main.m
//  PSO Mercury Soul
//
//  Created by Evan Shimizu on 2/24/13.
//  Copyright (c) 2013 CMU Drama. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([TSAppDelegate class]));
  }
}
