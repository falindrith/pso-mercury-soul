//
//  TSSettingsViewController.m
//
//  Created by Evan Shimizu on 2/28/13.
//

#import "TSSettingsViewController.h"

@interface TSSettingsViewController ()

@end

@implementation TSSettingsViewController

@synthesize connectionInfo = _connectionInfo, serverTextField = _serverTextField, portTextField = _portTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
	
  if (_connectionInfo)
  {
    _serverTextField.text = _connectionInfo.server;
    _portTextField.text = _connectionInfo.port;
  }
  else
  {
    _connectionInfo = [[TSConnectionModel alloc] init];
    [_connectionInfo loadData];
    _portTextField.text = _connectionInfo.port;
    _serverTextField.text = _connectionInfo.server;
  }
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  // Return YES for supported orientations
  if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
    return YES;
  }
  return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  
  return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([[segue identifier] isEqualToString:@"SettingsToVideo"]) {
    _connectionInfo = [[TSConnectionModel alloc] initWithServer: _serverTextField.text port:_portTextField.text];
    TSVideoViewController *videoViewController = [segue destinationViewController];
    [videoViewController setConnectionInfo: _connectionInfo];
    [_connectionInfo saveData];
  }
}

@end
