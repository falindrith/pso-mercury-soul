//
//  PSOMSConnectionModel.m
//  PSO Mercury Soul
//
//  Created by Evan Shimizu on 2/28/13.
//  Copyright (c) 2013 CMU Drama. All rights reserved.
//

#import "TSConnectionModel.h"

@implementation TSConnectionModel

@synthesize kApiKey = _kApiKey, kSessionId = _kSessionId, kToken = _kToken, server = _server, port = _port;

NSString* dataFilePath;

- (id)initWithServer:(NSString *)server port:(NSString *)port
{
  self = [super init];
  if (self)
  {
    _port = port;
    _server = server;
    
    return self;
  }
  return nil;
}

- (void)loadData
{
  NSFileManager *filemgr;
  NSString *docsDir;
  NSArray *dirPaths;
  
  filemgr = [NSFileManager defaultManager];
  
  // Get the documents directory
  dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  docsDir = [dirPaths objectAtIndex:0];
  
  // Build the path to the data file
  dataFilePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"data.archive"]];

  // Check if the file already exists
  if ([filemgr fileExistsAtPath: dataFilePath])
  {
    NSData *codedData = [[NSData alloc] initWithContentsOfFile: dataFilePath];
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData: codedData];
    _server = [unarchiver decodeObjectForKey: @"server"];
    _port = [unarchiver decodeObjectForKey:@"port"];
    [unarchiver finishDecoding];
  }
  else
  {
    _server = @"0.0.0.0";
    _port = @"10000";
  }
}

- (void)saveData
{
  NSMutableData *connectionData = [[NSMutableData alloc] init];
  NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData: connectionData];
  [archiver encodeObject:_server forKey:@"server"];
  [archiver encodeObject:_port forKey:@"port"];
  [archiver finishEncoding];
  [connectionData writeToFile:dataFilePath atomically:YES];
}
@end
