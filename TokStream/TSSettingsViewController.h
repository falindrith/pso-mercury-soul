//
//  PSOMSSettingsViewController.h
//
//  Created by Evan Shimizu on 2/28/13.
//

#import <UIKit/UIKit.h>
#import "TSConnectionModel.h"
#import "TSVideoViewController.h"

@interface TSSettingsViewController : UIViewController
<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *serverTextField;
@property (weak, nonatomic) IBOutlet UITextField *portTextField;
@property (nonatomic, strong) TSConnectionModel *connectionInfo;

@end
