//
//  TSAppDelegate.h
//
//  Created by Evan Shimizu on 2/24/13.
//

#import <UIKit/UIKit.h>

@interface TSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
