//
//  TSConnectionModel.h
//  TokStream
//
//  Created by Evan Shimizu on 2/28/13.
//

#import <Foundation/Foundation.h>

@interface TSConnectionModel : NSObject

@property (nonatomic, copy) NSString *kApiKey;
@property (nonatomic, copy) NSString *kSessionId;
@property (nonatomic, copy) NSString *kToken;
@property (nonatomic, copy) NSString *server;
@property (nonatomic, copy) NSString *port;

- (id)initWithServer:(NSString*)server port:(NSString*)port;
- (void)loadData;
- (void)saveData;
@end
