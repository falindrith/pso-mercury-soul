//
//  TSVideoViewController.m
//

#import "TSVideoViewController.h"

@implementation TSVideoViewController {
    OTSession* _session;
    OTPublisher* _publisher;
    OTSubscriber* _subscriber;
}

@synthesize connectionInfo = _connectionInfo, statusIndicator = _statusIndicator;

static double widgetHeight = 240;
static double widgetWidth = 320;
int screenWidth;
int screenHeight;

static NSString* const kApiKey = @"";    // Replace with your API Key
static NSString* const kSessionId = @""; // Replace with your generated Session ID
static NSString* const kToken = @"";     // Replace with your generated Token (use Project Tools or from a server-side library)

static bool subscribeToSelf = YES; // Change to NO if you want to subscribe to streams other than your own.

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self getApiInfo];
  
  screenHeight = [UIScreen mainScreen].bounds.size.height;
  screenWidth = [UIScreen mainScreen].bounds.size.width;
  
  [self setStatus:@"Connecting..."];
  
  _session = [[OTSession alloc] initWithSessionId:_connectionInfo.kSessionId
                                delegate:self];
  [self doConnect];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return NO;
    } else {
        return YES;
    }
}

- (void)updateSubscriber {
    for (NSString* streamId in _session.streams) {
        OTStream* stream = [_session.streams valueForKey:streamId];
        if (![stream.connection.connectionId isEqualToString: _session.connection.connectionId]) {
            _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
            break;
        }
    }
}

#pragma mark - Application methods

- (void)getApiInfo
{
  _connectionInfo.kApiKey = [self httpRequest:@"api_key"];
  _connectionInfo.kSessionId = [self httpRequest:@"session"];
  _connectionInfo.kToken = [self httpRequest:@"token"];
}

// Currently a test function that just always sends the same value to the bridge
// When this is actually working, we can send other values to change up what the brigde does
-(NSString*)httpRequest:(NSString*)resource
{
  NSString *url = [NSString stringWithFormat:@"http://%@:%@/%@", _connectionInfo.server, _connectionInfo.port, resource];
  
  NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
  [request setHTTPMethod:@"GET"];
  [request setURL:[NSURL URLWithString:url]];
  
  NSError *error = [[NSError alloc] init];
  NSHTTPURLResponse *responseCode = nil;
  
  NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
  
  if([responseCode statusCode] != 200){
    NSLog(@"Error getting %@, HTTP status code %i", url, [responseCode statusCode]);
    return NULL;
  }
  
  NSString *response = [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
  return response;
}

- (void)setStatus:(NSString*)message {
  _statusIndicator.text = message;
}


#pragma mark - OpenTok methods

- (void)doConnect 
{
    [_session connectWithApiKey:_connectionInfo.kApiKey token:_connectionInfo.kToken];
}

- (void)doPublish
{
  _publisher = [[OTPublisher alloc] initWithDelegate:self];
  [_publisher setCameraPosition: AVCaptureDevicePositionBack];
  [_publisher setPublishAudio: YES];
  [_publisher setName:[[UIDevice currentDevice] name]];
  [_session publish:_publisher];
  [self.view addSubview:_publisher.view];

  if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))
  {
    [_publisher.view setFrame:CGRectMake((screenHeight - widgetWidth) / 2, 0, widgetWidth, widgetHeight)];
  }
  else
  {
    [_publisher.view setFrame:CGRectMake(0, 50, widgetWidth, widgetHeight)];
  }
}

- (void)sessionDidConnect:(OTSession*)session
{
  NSLog(@"sessionDidConnect (%@)", session.sessionId);
  [self doPublish];
  [self setStatus:@"Connected to Session"];
}

- (void)sessionDidDisconnect:(OTSession*)session
{
  NSString* alertMessage = [NSString stringWithFormat:@"Session disconnected: (%@)", session.sessionId];
  NSLog(@"sessionDidDisconnect (%@)", alertMessage);
  [self setStatus:@"Session Disconnected. Reconnecting..."];
  
  _session = [[OTSession alloc] initWithSessionId:_connectionInfo.kSessionId
                                         delegate:self];
  [self doConnect];
}


- (void)session:(OTSession*)mySession didReceiveStream:(OTStream*)stream
{
    NSLog(@"session didReceiveStream (%@)", stream.streamId);

    // Client devices do not need to receive other streams. The connection is uni-directional
}

- (void)session:(OTSession*)session didDropStream:(OTStream*)stream{
    NSLog(@"session didDropStream (%@)", stream.streamId);
    NSLog(@"_subscriber.stream.streamId (%@)", _subscriber.stream.streamId);
    if (!subscribeToSelf
        && _subscriber
        && [_subscriber.stream.streamId isEqualToString: stream.streamId])
    {
        _subscriber = nil;
        [self updateSubscriber];
    }
}

- (void)subscriberDidConnectToStream:(OTSubscriber*)subscriber
{
    NSLog(@"subscriberDidConnectToStream (%@)", subscriber.stream.connection.connectionId);
}

- (void)publisher:(OTPublisher*)publisher didFailWithError:(OTError*) error {
    NSLog(@"publisher didFailWithError %@", error);
    [self showAlert:[NSString stringWithFormat:@"There was an error publishing."]];
}

- (void)subscriber:(OTSubscriber*)subscriber didFailWithError:(OTError*)error
{
    NSLog(@"subscriber %@ didFailWithError %@", subscriber.stream.streamId, error);
    [self showAlert:[NSString stringWithFormat:@"There was an error subscribing to stream %@", subscriber.stream.streamId]];
}

- (void)session:(OTSession*)session didFailWithError:(OTError*)error {
  NSLog(@"sessionDidFail");
  [self showAlert:[NSString stringWithFormat:@"There was an error connecting to session %@. Chcek your network settings.", session.sessionId]];
  [self setStatus:@"Connection Error!"];
}


- (void)showAlert:(NSString*)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message from video session"
                                                    message:string
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
  if (UIInterfaceOrientationIsLandscape(fromInterfaceOrientation))
  {
    // In portrait now
    [_publisher.view setFrame:CGRectMake(0, 50, widgetWidth, widgetHeight)];
  }
  else
  {
    [_publisher.view setFrame:CGRectMake((screenHeight - widgetWidth) / 2, 0, widgetWidth, widgetHeight)];
  }
  
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([[segue identifier] isEqualToString:@"VideoToSettings"]) {
    [_session unpublish:_publisher];
    
    TSSettingsViewController *settingsViewController = [segue destinationViewController];
    [settingsViewController setConnectionInfo: _connectionInfo];
  }
}

@end

