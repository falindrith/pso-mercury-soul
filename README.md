PSO Mercury Soul
================

Live video component of the Pittsburgh production of Mercury Soul.

This contains a mobile iOS client that streams video using OpenTok
that's received by a web-client that allows a user to select between
desired streams at will. The iOS client stream is audio-less and
uni-directional. The web client does not broadcast and only receives
the video streams.
